# wrapper for ssh setup for statichosts
class staticsync::ssh  (
  Variant[Array[String], String] $add_tag,
  String                         $collect_tag,
) {
  ssh::keygen { $staticsync::user : }

  ssh::authorized_key_add { "staticsync-${staticsync::user}":
    target_user => $staticsync::user,
    key         => dig($facts, 'ssh_keys_users', $staticsync::user, 'id_rsa.pub', 'line'),
    command     => "/usr/local/bin/staticsync-ssh-wrap ${::fqdn}",
    options     => ['restrict','pty'],
    from        => $staticsync::public_addresses,
    collect_tag => $add_tag,
  }
  ssh::authorized_key_collect { "staticsync-${staticsync::user}":
    target_user => $staticsync::user,
    collect_tag => $collect_tag,
  }
}
