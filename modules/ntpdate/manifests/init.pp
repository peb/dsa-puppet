class ntpdate {
  package { [
    'ntpdate',
    'lockfile-progs'
  ]:
    ensure => installed
  }

  $ntpservers = $::hostname ? {
    default => ['manda-node03.debian.org', 'manda-node04.debian.org', 'conova-node03.debian.org', 'conova-node04.debian.org']
  }

  file { '/etc/default/ntpdate':
    content => template('ntpdate/etc-default-ntpdate.erb'),
  }
}
