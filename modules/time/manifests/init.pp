class time {
	include stdlib
	$localtimeservers = hiera('local-timeservers', [])
	$physicalHost = $deprecated::allnodeinfo[$fqdn]['physicalHost']
	$ntp_use_local_timeservers = hiera('ntp::use_local_timeservers', false)

	if ($ntp_use_local_timeservers and size($localtimeservers) > 0) {
		include ntp
	#if ($systemd and $physicalHost and size($localtimeservers) > 0) {
	} elsif ($systemd and size($localtimeservers) > 0 and $::is_virtual and $::virtual == 'kvm') {
		include ntp::purge
		include systemdtimesyncd
	} else {
		include ntp
	}
}
