class acpi {
	package { 'acpid':
		ensure => purged
	}

	package { 'acpi-support-base':
		ensure => purged
	}
}
