# Install and configure systemd-timesyncd
class systemdtimesyncd {
  $localtimeservers = hiera('local-timeservers', [])

  if (! $::systemd) {
    fail ( 'systemdtimesyncd requires systemd.' )
  } elsif (size($localtimeservers) == 0) {
    fail ( 'No local timeservers configured for systemdtimesyncd.' )
  } else {
    if ($::systemd_ge_245_4_2) {
      ensure_packages(['systemd-timesyncd'], { ensure => 'installed' })
    }
    file { '/etc/systemd/system/sysinit.target.wants/systemd-timesyncd.service':
        ensure => 'link',
        target => '/lib/systemd/system/systemd-timesyncd.service',
        notify => Exec['systemctl daemon-reload'],
    }

    service { 'systemd-timesyncd':
      ensure  => running,
    }

    file { '/etc/systemd/timesyncd.conf':
      content => template('systemdtimesyncd/timesyncd.conf.erb'),
      notify  => Service['systemd-timesyncd'],
    }

    # Up until 2019-09 we had timesyncd started by multi-user.target instead of the default
    # sysinit.target.wants.  We are moving back to sysinit.target.wants (for now).
    file { '/etc/systemd/system/multi-user.target.wants/systemd-timesyncd.service':
      ensure => 'absent',
      notify => Exec['systemctl daemon-reload'],
    }

  }
}
