# our heavy exim class
# @param is_mailrelay this system is a mailrelay, both in and out, for debian hosts
class exim::mx(
  Boolean $is_mailrelay = false,
){
  class { 'exim':
    use_smarthost => false,
    is_mailrelay  => $is_mailrelay,
  }

  include clamav
  include postgrey
  include fail2ban::exim

  file { '/etc/exim4/ccTLD.txt':
    source => 'puppet:///modules/exim/common/ccTLD.txt',
  }
  file { '/etc/exim4/surbl_whitelist.txt':
    source => 'puppet:///modules/exim/common/surbl_whitelist.txt',
  }
  file { '/etc/exim4/two-level-tlds':
    source => 'puppet:///modules/exim/common/two-level-tlds',
  }
  file { '/etc/exim4/three-level-tlds':
    source => 'puppet:///modules/exim/common/three-level-tlds',
  }
  file { '/etc/exim4/exim_surbl.pl':
    source => 'puppet:///modules/exim/common/exim_surbl.pl',
    notify => Service['exim4'],
  }

  package { 'monitoring-plugins-standard':
    ensure => installed,
  }

  ferm::rule::simple { 'dsa-smtp':
    description => 'Allow smtp access from the world',
    port        => '25',
  }
}
