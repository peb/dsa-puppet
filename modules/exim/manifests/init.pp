# our exim class
# @param use_smarthost use the smarthost
# @param smarthost host to relay through (if set and use_smarthost)
# @param is_bugsmx this system handles bugs.debian.org
# @param is_mailrelay this system is a mailrelay, both in and out, for debian hosts
# @param is_rtmaster this system handles rt.debian.org
# @param is_packagesmaster this system handles packages.debian.org
# @param is_packagesqamaster this system handles packages.qa.debian.org
# @param is_trackermaster this system handles tracker.debian.org
# @param mail_port override the default incoming mailport (only applicable for hosts behind an incoming relay)
# @param smarthost_port the port on which satellites send mail to the smarthost (needs to be the same on both sides)
class exim (
  Optional[String] $smarthost,
  Boolean $use_smarthost = true,
  Boolean $is_bugsmx = false,
  Boolean $is_mailrelay = false,
  Boolean $is_rtmaster = false,
  Boolean $is_packagesmaster = false,
  Boolean $is_packagesqamaster = false,
  Boolean $is_trackermaster = false,
  Integer $smarthost_port = 587,
  Optional[Integer] $mail_port = undef,
) {
  $le_dir = hiera('paths.letsencrypt_dir')
  $have_host_le = (file("${le_dir}/${fqdn}.key", '/dev/null') != '')
  ssl::host_le{"exim-${fqdn}":
    ensure           =>  if ($is_mailrelay) { 'present' } else { 'absent' },
    path_crt         => undef,
    path_crt_chained => '/etc/exim4/ssl/thishost-le.crt',
    path_key         => '/etc/exim4/ssl/thishost-le.key',
    key_group        => 'Debian-exim',
  }

  if $use_smarthost {
    $heavy = false

    if ! smarthost {
      fail('No smarthost set but use_smarthost is true')
    }
  } else {
    $heavy = true
    if $mail_port {
      fail('Cannot override mail_port in heavy/no-smarthost hosts')
    }
  }

  file { '/usr/share/munin/plugins/dsa_exim_mailstats':
    ensure => file,
    source => 'puppet:///modules/exim/common/exim_mailstats',
    mode   => '0755',
  }
  munin::check { 'ps_exim4': script => 'ps_' }
  munin::check { 'exim_mailqueue': }
  munin::check { 'exim_mailstats': ensure => absent }
  munin::check { 'dsa_exim_mailstats': }

  munin::check { 'postfix_mailqueue':  ensure => absent }
  munin::check { 'postfix_mailstats':  ensure => absent }
  munin::check { 'postfix_mailvolume': ensure => absent }

  package { 'exim4-daemon-heavy': ensure => installed }

  Package['exim4-daemon-heavy']->Mailalias<| |>

  service { 'exim4':
    ensure  => running,
    require => [
      File['/etc/exim4/exim4.conf'],
      Package['exim4-daemon-heavy'],
    ]
  }

  file { '/etc/exim4/':
    ensure  => directory,
    mode    => '0755',
    require => Package['exim4-daemon-heavy'],
    purge   => true,
  }
  file { '/etc/exim4/conf.d':
    ensure  => directory,
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///files/empty/',
  }
  file { '/etc/exim4/ssl':
    ensure => directory,
    group  => 'Debian-exim',
    mode   => '0750',
    purge  => true,
  }
  file { '/etc/exim4/exim4.conf':
    content => template('exim/eximconf.erb'),
    require => File['/etc/exim4/ssl/thishost.crt'],
    notify  => Service['exim4'],
  }
  file { '/etc/mailname':
    content => template('exim/mailname.erb'),
  }
  file { '/etc/default/exim4':
    content => template('exim/etc-default-exim4.erb'),
    notify  => Service['exim4'],
  }

  concat { '/etc/exim4/virtualdomains': }
  concat::fragment { 'virtualdomains_header':
    target  => '/etc/exim4/virtualdomains',
    content => template('exim/virtualdomains.header.erb'),
    order   => '00',
  }

  file { '/etc/exim4/locals':
    content => template('exim/locals.erb')
  }

  concat { '/etc/exim4/submission-domains':
    ensure_newline => true,
    warn           => @(EOF),
    ## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
    | EOF
  }

  concat { '/etc/exim4/dkim-domains':
    ensure_newline => true,
    warn           => @(EOF),
    ## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
    | EOF
  }

  file { '/etc/exim4/host_blocklist':
    source => 'puppet:///modules/exim/common/host_blocklist',
  }
  file { '/etc/exim4/blocklist':
    source => 'puppet:///modules/exim/common/blocklist',
  }
  file { '/etc/exim4/host_blacklist':
    ensure => absent,
  }
  file { '/etc/exim4/blacklist':
    ensure => absent,
  }
  file { '/etc/exim4/callout_users':
    source => 'puppet:///modules/exim/common/callout_users',
  }
  file { '/etc/exim4/grey_users':
    source => 'puppet:///modules/exim/common/grey_users',
  }
  file { '/etc/exim4/helo-check':
    source => 'puppet:///modules/exim/common/helo-check',
  }
  file { '/etc/exim4/localusers':
    source => 'puppet:///modules/exim/common/localusers',
  }
  file { '/etc/exim4/rbllist':
    source => 'puppet:///modules/exim/common/rbllist',
  }
  file { '/etc/exim4/rhsbllist':
    source => 'puppet:///modules/exim/common/rhsbllist',
  }
  file { '/etc/exim4/whitelist':
    source => 'puppet:///modules/exim/common/whitelist',
  }
  file { '/etc/logrotate.d/exim4-base':
    source => 'puppet:///modules/exim/common/logrotate-exim4-base',
  }
  file { '/etc/logrotate.d/exim4-paniclog':
    source => 'puppet:///modules/exim/common/logrotate-exim4-paniclog'
  }
  file { '/etc/exim4/ssl/thishost.crt':
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @fqdn + ".crt") %>'),
    group   => 'Debian-exim',
    mode    => '0640',
  }
  file { '/etc/exim4/ssl/thishost.key':
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @fqdn + ".key") %>'),
    group   => 'Debian-exim',
    mode    => '0640',
  }
  file { '/etc/exim4/ssl/dkim.key':
    source  => '/etc/ssl/private/dkim-host-auto.key',
    group   => 'Debian-exim',
    mode    => '0640',
    require => Exec['create-rsa-ssl-key-dkim-host-auto'],
  }
  file { '/etc/exim4/ssl/ca.crt':
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/ca.crt") %>'),
    group   => 'Debian-exim',
    mode    => '0640',
  }
  file { '/etc/exim4/ssl/ca.crl':
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/ca.crl") %>'),
    group   => 'Debian-exim',
    mode    => '0640',
  }
  file { '/var/log/exim4':
    ensure => directory,
    mode   => '2750',
    owner  => 'Debian-exim',
    group  => maillog,
  }

  # These only affect the alias @$fqdn, not say, @debian.org

  mailalias { [
    'postmaster',
    'hostmaster',
    'usenet',
    'webmaster',
    'abuse',
    'noc',
    'security',
  ]:
    ensure => absent
  }
}
