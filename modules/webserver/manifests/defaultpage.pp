# Install default website content
#
# @param defaultdomain domain name of the default page, to create directory under /srv/www
class webserver::defaultpage (
  String $defaultdomain,
) {
  file { [ '/srv/www', "/srv/www/${defaultdomain}", "/srv/www/${defaultdomain}/htdocs", "/srv/www/${defaultdomain}/htdocs-disabled" ]:
    ensure => directory,
    mode   => '0755',
  }

  file { "/srv/www/${defaultdomain}/htdocs/index.html":
    content => template('webserver/default-index.html'),
  }

  file { "/srv/www/${defaultdomain}/htdocs-disabled/index.html":
    content => template('webserver/disabled-index.html'),
  }
}
