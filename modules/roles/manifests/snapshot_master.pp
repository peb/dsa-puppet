# snapshot master
class roles::snapshot_master {
  include roles::snapshot_base
  include roles::snapshot_ssh_keygen

  ssh::authorized_key_add { 'roles::snapshot_master::to::farmsync_target':
    target_user => 'snapshot',
    key         => dig($facts, 'ssh_keys_users', 'snapshot', 'id_rsa.pub', 'line'),
    command     => '~/bin/run-sync',
    collect_tag => 'roles::snapshot::to::farmsync_target',
  }

  ssh::authorized_key_collect { 'snapshot':
    target_user => 'snapshot',
    collect_tag => 'roles::snapshot::to::master',
  }
}
