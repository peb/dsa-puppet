class roles::ssh_upload {
  ssh::authorized_key_collect { 'buildd-uploader':
    target_user => 'buildd-uploader',
    collect_tag => 'buildd_upload',
  }

  file { '/home/buildd-uploader/rsync-ssh-wrap':
    source => 'puppet:///modules/roles/ssh_upload/rsync-ssh-wrap',
    mode   => '0555',
  }
}
