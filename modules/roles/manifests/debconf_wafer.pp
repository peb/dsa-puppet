# debconf's wafer role

# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::debconf_wafer (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include apache2::ssl
  include apache2::expires

  include roles::sso_rp

  package { 'libapache2-mod-wsgi-py3': ensure => installed, }
  apache2::module { 'wsgi': require => Package['libapache2-mod-wsgi-py3'] }

  ssl::service { 'wafertest.debconf.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '010-wafertest.debconf.org':
    site   => 'wafertest.debconf.org',
    source => 'puppet:///modules/roles/debconf_wafer/wafertest.debconf.org',
  }

  ssl::service { 'debconf18.debconf.org':
    ensure => absent,
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '010-debconf18.debconf.org':
    ensure => absent,
    site   => 'debconf18.debconf.org',
  }

  ssl::service { 'debconf19.debconf.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '010-debconf19.debconf.org':
    site   => 'debconf19.debconf.org',
    source => 'puppet:///modules/roles/debconf_wafer/debconf19.debconf.org',
  }

  ssl::service { 'debconf20.debconf.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '010-debconf20.debconf.org':
    site   => 'debconf20.debconf.org',
    source => 'puppet:///modules/roles/debconf_wafer/debconf20.debconf.org',
  }

  ssl::service { 'debconf21.debconf.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '010-debconf21.debconf.org':
    site   => 'debconf21.debconf.org',
    source => 'puppet:///modules/roles/debconf_wafer/debconf21.debconf.org',
  }

  ssl::service { 'debconf22.debconf.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '010-debconf22.debconf.org':
    site   => 'debconf22.debconf.org',
    source => 'puppet:///modules/roles/debconf_wafer/debconf22.debconf.org',
  }

  @@postgres::cluster::hba_entry { "debconf-wafer-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    user     => ['debconf18', 'debconf19', 'debconf20', 'debconf21', 'debconf22', 'wafertest'],
    database => 'sameuser',
    address  => $base::public_addresses,
  }

  exim::dkimdomain { 'debconf.org': }
}

