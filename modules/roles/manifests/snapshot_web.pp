# web service for snapshot.debian.org
#
# @param ipv4addr IPv4 address for the snapshot service
# @param ipv6addr IPv6 address for the snapshot service
# @param ipv6addr_apache IPv6 address apache should listen on
# @param packages List of packages to install (e.g. apache module)
class roles::snapshot_web(
  Stdlib::IP::Address::V4::Nosubnet $ipv4addr,
  Stdlib::IP::Address::V6::Nosubnet $ipv6addr,
  Stdlib::IP::Address::V6::Nosubnet $ipv6addr_apache,
  Array[String] $packages = ['libapache2-mod-wsgi'],
) {
  include apache2
  include apache2::rewrite

  # snapshot abusers
  #  61.69.254.110 - 20180705, mirroring with wget
  # 20180821 large amount of requests way too fast from some amazon AWS instances
  #  18.185.157.46
  #  18.194.174.202
  #  18.184.181.169
  #  18.184.5.230
  #  18.194.137.96
  #  18.197.147.183
  #  3.120.39.137
  #  3.120.41.69
  #  35.158.129.130
  #  52.59.199.25
  #  52.59.228.158
  #  52.59.245.42
  #  52.59.253.41
  #  52.59.71.13
  # 20180821 mirroring
  #  99.137.191.34
  # 20181110 crawler
  #  51.15.215.91
  # 20181222, excessive number of requests
  #  208.91.68.213
  # 198.11.128.0/18
  # running jigdo against snapshot
  #  159.226.95.0/24
  #  84.204.194.0/24
  #  211.13.205.0/24
  # 20190512 tens of thousands of queries
  #  63.32.0.0/14
  #  54.72.0.0/15
  #  95.115.66.23
  #  52.192.0.0/11
  #  54.72.0.0/15
  #  34.192.0.0/10
  #  34.240.0.0/13
  #  52.192.0.0/11
  #  90.44.107.223
  #  195.154.173.12
  #  74.121.137.108
  ferm::rule { 'dsa-snapshot-abusers':
    prio => '005',
    rule => 'saddr (61.69.254.110 99.137.191.34 51.15.215.91 208.91.68.213 198.11.128.0/18 159.226.95.0/24 84.204.194.0/24 211.13.205.0/24 95.115.66.23 90.44.107.223 195.154.173.12 74.121.137.108) DROP',
  }

  # rate limit accesses.  The chain is set up by the apache module and allow happens at prio 90.
  ferm::rule { 'dsa-http-snapshot-limit':
    prio        => '22',
    description => 'rate limit for snapshot',
    chain       => 'http',
    domain      => '(ip ip6)',
    rule        => '
        mod hashlimit hashlimit-name HTTPDOSPRE hashlimit-mode srcip hashlimit-burst 10 hashlimit 6/minute jump ACCEPT;
        mod recent name HTTPDOS update seconds 900 jump log_or_drop;
        mod hashlimit hashlimit-name HTTPDOS hashlimit-mode srcip hashlimit-burst 200 hashlimit 30/minute jump ACCEPT;
        mod recent name HTTPDOS set jump log_or_drop'
  }

  ensure_packages( $packages, {
    ensure => 'installed',
  })

  apache2::site { '020-snapshot.debian.org':
    site    => 'snapshot.debian.org',
    content => template('roles/snapshot/snapshot.debian.org.conf.erb')
  }

  ferm::rule { 'dsa-snapshot-connlimit':
    domain => '(ip ip6)',
    prio   => '005',
    rule   => "proto tcp mod state state (NEW) interface ! lo daddr (${ipv4addr} ${ipv6addr})  mod multiport destination-ports (80 443) mod connlimit connlimit-above 3 DROP;
               proto tcp mod state state (NEW) interface ! lo                                                dport 6081                 mod connlimit connlimit-above 3 DROP
               ",
  }

  # varnish cache
  ###############
  ferm::rule { 'dsa-nat-snapshot-varnish-v4':
    table => 'nat',
    chain => 'PREROUTING',
    rule  => "proto tcp daddr ${ipv4addr} dport 80 REDIRECT to-ports 6081",
  }

  varnish::config { 'default':
    listen  => [
      ':6081',
      "[${ipv6addr}]:80"
      ],
    backend => 'file,/var/lib/varnish/varnish_storage.bin,8G',
    content => template('roles/snapshot/snapshot.debian.org.vcl.erb'),
  }

  # the ipv6 port 80 is owned by varnish
  file { '/etc/apache2/ports.conf':
    content => @("EOF"),
      Listen 0.0.0.0:80
      Listen [${ipv6addr_apache}]:80
      | EOF
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  # haproxy ssl termination
  #########################
  include haproxy
  file { '/etc/haproxy/haproxy.cfg':
    content => template('roles/snapshot/haproxy.cfg.erb'),
    require => Package['haproxy'],
    notify  => Service['haproxy'],
  }
  ssl::service { 'snapshot.debian.org':
    notify => Service['haproxy'],
    key    => true,
  }
}
