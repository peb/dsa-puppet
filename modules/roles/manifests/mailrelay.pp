# = Class: roles::mailrelay
#
# Setup for mailrelay hosts
#
# == Sample Usage:
#
#   include roles::mailrelay
#
class roles::mailrelay {
  include exim::mailrelay

  include roles::pubsub::parameters

  $rabbit_password = $roles::pubsub::parameters::rabbit_password

  roles::pubsub::config { 'emailvdomains':
    key      => 'dsa-emailvdomains-receive',
    exchange => dsa,
    queue    => "email-${::fqdn}",
    topic    => 'dsa.email.update',
    vhost    => dsa,
    username => $::fqdn,
    password => $rabbit_password
  }

  # smtp firewalling setup
  ###
  @@ferm::rule::simple { "dsa-smtp-from-mailrelay-${::fqdn}":
    tag         => 'smtp::server::to::mail-satellite',
    description => 'Allow smtp access from a mailrelay',
    port        => '7', # will be overwritten on collection
    saddr       => $base::public_addresses,
  }

  ferm::rule::simple { 'submission-from-satellites':
    target => 'submission',
    port   => 'submission',
  }
  Ferm::Rule::Simple <<| tag == 'smtp::server::submission::to::mail-relay' |>>

  $autocertdir = hiera('paths.auto_certs_dir')
  dnsextras::tlsa_record{ 'tlsa-submission':
    zone     => 'debian.org',
    certfile => "${autocertdir}/${::fqdn}.crt",
    port     => 587,
    hostname => $::fqdn,
  }

  $le_dir = hiera('paths.letsencrypt_dir')
  $le_crt_fn = "${le_dir}/${::fqdn}.crt"
  if (file($le_crt_fn, '/dev/null') != '') {
    dnsextras::tlsa_record{ 'tlsa-submission-le':
      zone     => 'debian.org',
      certfile => $le_crt_fn,
      port     => 587,
      hostname => $::fqdn,
    }
  }
}
