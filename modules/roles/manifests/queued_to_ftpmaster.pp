# ftp-master runs scripts that need access to the dak DB replica
# and send mail from @ftp-master.d.o
#
# Hosts that run upload queues (ftp or ssh), for packages destined
# for ftp-master.
class roles::queued_to_ftpmaster {
  include roles::postgresql::ftp_master_dak_replica::db_guest_access::ubc

  exim::dkimdomain { 'ftp-master.debian.org': }
}
