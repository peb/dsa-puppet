# a static web mirror
#
# @param listen_addr IP addresses to have apache listen on
# @param onion_service provide the onion service from this host
class roles::static_mirror_web(
  Array[Stdlib::IP::Address] $listen_addr = [],
  Boolean $onion_service = false,
) {
  $_enclosed_addresses = empty($listen_addr) ? {
    true    => ['*'],
    default => enclose_ipv6($listen_addr),
  }
  $vhost_listen     = $_enclosed_addresses.map |$a| { "${a}:80"  } .join(' ')
  $vhost_listen_443 = $_enclosed_addresses.map |$a| { "${a}:443" } .join(' ')

  include roles::static_mirror
  include roles::weblog_provider

  include apache2
  include apache2::expires
  include apache2::rewrite

  package { 'libapache2-mod-geoip': ensure => installed, }
  package { 'geoip-database': ensure => installed, }

  include apache2::ssl
  apache2::module { 'include': }
  apache2::module { 'geoip': require => [Package['libapache2-mod-geoip'], Package['geoip-database']]; }
  apache2::module { 'deflate': }
  apache2::module { 'filter': }

  $redirect_vhosts = true

  apache2::config { 'local-static-vhost.conf':
    ensure => absent,
  }
  apache2::config { 'local-static-vhost':
    content => template('roles/static-mirroring/static-vhost.conf.erb'),
  }

  apache2::site { '010-planet.debian.org':
    ensure  => has_static_component('planet.debian.org') ? { true => 'present', false => 'absent' },
    site    => 'planet.debian.org',
    content => template('roles/static-mirroring/vhost/planet.debian.org.erb'),
  }

  apache2::site { '010-static-vhosts-00-manpages':
    ensure  => has_static_component('manpages.debian.org') ? { true => 'present', false => 'absent' },
    site    => 'static-manpages.debian.org',
    content => template('roles/static-mirroring/vhost/manpages.debian.org.erb'),
  }
  apache2::site { '010-static-vhosts-simple':
    site    => 'static-vhosts-simple',
    content => template('roles/static-mirroring/vhost/static-vhosts-simple.erb'),
  }

  $wwwdo_server_name = 'www.debian.org'
  $wwwdo_document_root = '/srv/static.debian.org/mirrors/www.debian.org/cur'
  apache2::site { '005-www.debian.org':
    ensure  => has_static_component('www.debian.org') ? { true => 'present', false => 'absent' },
    site    => 'www.debian.org',
    content => template('roles/apache-www.debian.org.erb'),
  }

  ssl::service { 'www.debian.org'      : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debian.org' :
    ensure => has_static_component('www.debian.org') ? { true => 'present', false => 'absent' },
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  # do
  ssl::service { 'appstream.debian.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'backports.debian.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'bits.debian.org'               : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'blends.debian.org'             : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'cdbuilder-logs.debian.org'     : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true }
  ssl::service { 'd-i.debian.org'                : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true }
  ssl::service { 'deb.debian.org'                : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, tlsaport => [] }
  ssl::service { 'dpl.debian.org'                : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true }
  ssl::service { 'dsa.debian.org'                : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true }
  ssl::service { 'incoming.debian.org'           : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'incoming.ports.debian.org'     : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'lists.alioth.debian.org'       : ensure => 'present',  notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'manpages.debian.org'           : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'metadata-backend.ftp-master.debian.org':
    ensure => has_static_component('metadata.ftp-master.debian.org') ? { true => 'present', false => 'absent' },
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  ssl::service { 'mirror-master.debian.org'      : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'onion.debian.org'              : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'openpgpkey.debian.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'pkg-ruby-extras.alioth.debian.org' : ensure => 'present',  notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'planet-backend.debian.org'     :
    ensure => has_static_component('planet.debian.org') ? { true => 'present', false => 'absent' },
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  ssl::service { 'release.debian.org'            : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'security-team.debian.org'      : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'www.ports.debian.org'          : ensure => 'present', notify  => Exec['service apache2 reload'], key => true, }
  # dn
  ssl::service { 'bootstrap.debian.net'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debaday.debian.net'            : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debdeltas.debian.net'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'micronews.debian.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'mozilla.debian.net'            : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'news.debian.net'               : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'sources.debian.net'            : ensure => 'present',  notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'timeline.debian.net'           : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'video.debian.net'              : ensure => 'present',  notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'wnpp-by-tags.debian.net'       : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  # dc
  ssl::service { 'debconf.org'                   : ensure => 'present', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { '10years.debconf.org'           : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf0.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf1.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf2.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf3.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf4.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf5.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf6.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf7.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf8.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf9.debconf.org'          : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf10.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf11.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf12.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf13.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf14.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf15.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf16.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf17.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf18.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf19.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'debconf20.debconf.org'         : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'es.debconf.org'                : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'fr.debconf.org'                : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'lists.debconf.org'             : ensure => 'present',  notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'media.debconf.org'             : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'miniconf10.debconf.org'        : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'video.debconf.org'             : ensure => 'present',  notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'wiki.debconf.org'              : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }
  include apache2::proxy_http
  ssl::service { 'www.debconf.org'               : ensure => 'ifstatic', notify  => Exec['service apache2 reload'], key => true, }


  if $onion_service {
    $onion_addr = empty($listen_addr) ? {
      true    => $base::public_address,
      default => filter_ipv4($listen_addr)[0]
    }
    if ! $onion_addr {
      fail("Do not have a useable address for the onionservice on ${::hostname}.  Is \$listen_addr empty or does it not have an IPv4 address?.")
    }

    onion::service { 'd-i.debian.org'      : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'dpl.debian.org'      : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'dsa.debian.org'      : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'www.debian.org'      : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }

    # do
    onion::service { 'appstream.debian.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'backports.debian.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'bits.debian.org'               : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'blends.debian.org'             : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'cdbuilder-logs.debian.org'     : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'incoming.debian.org'           : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'incoming.ports.debian.org'     : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'manpages.debian.org'           : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'mirror-master.debian.org'      : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'onion.debian.org'              : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'openpgpkey.debian.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'release.debian.org'            : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'security-team.debian.org'      : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'www.ports.debian.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    # dn
    onion::service { 'bootstrap.debian.net'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debaday.debian.net'            : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debdeltas.debian.net'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'micronews.debian.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'mozilla.debian.net'            : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'news.debian.net'               : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'timeline.debian.net'           : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'wnpp-by-tags.debian.net'       : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    # dc
    onion::service { '10years.debconf.org'           : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf0.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf1.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf10.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf11.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf12.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf13.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf14.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf15.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf16.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf17.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf18.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf19.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf20.debconf.org'         : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf2.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf3.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf4.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf5.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf6.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf7.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf8.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'debconf9.debconf.org'          : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'es.debconf.org'                : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'fr.debconf.org'                : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'media.debconf.org'             : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'miniconf10.debconf.org'        : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }

    # non-SSL
    onion::service { 'metadata.ftp-master.debian.org': ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
    onion::service { 'planet.debian.org'             : ensure => 'ifstatic', port => 80, target_port => 80, target_address => $onion_addr }
  }

  file { '/srv/static.debian.org/puppet':
    ensure => directory,
    mode   => '2755'
  }
  file { '/srv/static.debian.org/puppet/disabled-service':
    ensure => directory,
    mode   => '2755'
  }
  file { '/srv/static.debian.org/puppet/disabled-service/503.html':
    source => 'puppet:///modules/roles/static-htdocs/disabled-service/503.html',
  }

  ssl::service { 'archive.debian.net': ensure => present, notify  => Exec['service apache2 reload'], key => true, }
  file { '/srv/static.debian.org/puppet/archive.debian.net':
    ensure => absent,
  }
  file { '/srv/static.debian.org/puppet/archive.debian.net/503.html':
    ensure => absent,
  }

  ssl::service { 'cdimage.debian.org': ensure => present, notify => Exec['service apache2 reload'], key => true, tlsaport => [], }
  file { '/srv/static.debian.org/puppet/cdimage.debian.org':
    ensure => directory,
  }
  file { '/srv/static.debian.org/puppet/cdimage.debian.org/503.html':
    source => 'puppet:///modules/roles/static-htdocs/cdimage-maintenance.html',
  }
}
