# ddtp.debian.org service
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::ddtp (
  String  $db_address,
  Integer $db_port,
) {
  include apache2

  ssl::service { 'ddtp.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  onion::service { 'ddtp.debian.org': port => 80, target_address => 'ddtp.debian.org', target_port => 80, direct => true }

  @@postgres::cluster::hba_entry { "ddtp-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => ['ddtp'],
    user     => 'ddtp',
    address  => $base::public_addresses,
  }

  package { 'debian.org-ddtp.debian.org': ensure => installed, }
}
