# postgresql cluster configuration
#
# @param pg_version      pg version of the cluster
# @param pg_cluster      cluster name
# @param pg_port         port of the postgres cluster
# @param manage_hba      manage pg_hba
# @param confdir         directory where the configuration resides
# @param backups         make backups of this cluster (unless it is recovering/a replication target)
# @param monitor_dbs     list of databases to monitor (default is all non-template non-default databases). Can be a boolean to indicate no / all databases
define postgres::cluster(
  Optional[Integer] $pg_port = undef,
  Optional[String] $pg_cluster = undef,
  Optional[String] $pg_version = undef,
  Boolean $manage_hba = false,
  String $confdir = "/etc/postgresql/${pg_version}/${pg_cluster}",
  Boolean $backups = true,
  Optional[Variant[Boolean,Array[String]]] $monitor_dbs = undef,
) {

  if ! $manage_hba {
    fail('Variable $manage_hba is set to false.  We are trying to migrate all our clusters to having hba managed by puppet, so please report this issue and/or migrate this cluster.')
  }

  # get remaining cluster info and verify consistency
  ###
  $clusters = $facts['postgresql_clusters']
  if $pg_port {
    $filtered = $clusters.filter |$cluster| { $cluster['port'] == $pg_port }
    if $filtered.length != 1 {
      fail("Did not find exactly one cluster with port ${pg_port}")
    }
    $cluster = $filtered[0]
  } elsif $pg_cluster and $pg_version {
    $filtered = $clusters.filter |$cluster| { $cluster['version'] == $pg_version and $cluster['cluster'] == $pg_cluster}
    if $filtered.length != 1 {
      fail("Did not find exactly one cluster ${pg_version}/${pg_cluster}")
    }
    $cluster = $filtered[0]
  } else {
    fail('postgres::cluster::hba_entry needs either the port of both a pg version and cluster name')
  }
  $real_port    = $cluster['port']
  $real_version = $cluster['version']
  $real_cluster = $cluster['cluster']
  if $pg_version and $pg_version != $real_version {
    fail("Inconsisten cluster version information: ${pg_version} != ${real_version}")
  }
  if $pg_cluster and $pg_cluster != $real_cluster {
    fail("Inconsisten cluster name information: ${pg_cluster} != ${real_cluster}")
  }
  ###

  # basic infra
  ###
  $reload = "postgresql ${real_version}/${real_cluster} reload"
  exec { $reload:
    command     => "systemctl reload postgresql@${real_version}-${real_cluster}.service",
    refreshonly => true,
    subscribe   => File['/etc/ssl/debian/certs/thishost-server.crt'],
  }
  ferm::rule::chain { "postgres::cluster::hba_entry::chain::pg-${real_port}":
    description => "chain for pg${real_version}/${real_cluster}",
    chain       => "pg-${real_port}",
  }
  ferm::rule::simple { "postgres::cluster::hba_entry::${real_version}::${real_cluster}":
    description => "check access to pg${real_version}/${real_cluster}",
    port        => $real_port,
    target      => "pg-${real_port}",
  }
  ###

  munin::check { "postgres_bgwriter_${real_version}_${real_cluster}":
    script => 'postgres_wrapper',
  }
  munin::check { "postgres_connections_db_${real_version}_${real_cluster}":
    script => 'postgres_wrapper',
  }
  munin::check { "postgres_cache_ALL_${real_version}_${real_cluster}":
    script => 'postgres_wrapper',
  }
  munin::check { "postgres_querylength_ALL_${real_version}_${real_cluster}":
    script => 'postgres_wrapper',
  }
  munin::check { "postgres_size_ALL_${real_version}_${real_cluster}":
    script => 'postgres_wrapper',
  }
  munin::check { "postgres_wal_traffic_${real_version}_${real_cluster}":
    script => 'postgres_wal_traffic_'
  }

  $filtered_dbs = $cluster['databases'].filter |$database| { !$database['istemplate'] and $database['allowconn'] and $database['name'] != 'postgres' }
  if $monitor_dbs == undef {
    if $filtered_dbs.length > 1 {
      $databases = $filtered_dbs
    } else {
      $databases = []
    }
  } elsif $monitor_dbs =~ Boolean and $monitor_dbs {
    $databases = $filtered_dbs
  } elsif $monitor_dbs =~ Array[String] and $monitor_dbs {
    # explicit list of databases
    $databases = $monitor_dbs
  } else {
    $databases = []
  }

  $databases.each |$database| {
    if $database =~ Hash {
      $real_database = $database['name']
    } else {
      $real_database = $database
    }
    munin::check { "postgres_querylength_${real_database}_${real_version}_${real_cluster}":
      script => 'postgres_wrapper',
    }
    munin::check { "postgres_size_${real_database}_${real_version}_${real_cluster}":
      script => 'postgres_wrapper',
    }
  }

  if $backups {
    if !$cluster['status']['recovery'] {
      postgres::backup_cluster { "${real_version}::${real_cluster}":
        pg_version => $real_version,
        pg_cluster => $real_cluster,
        pg_port    => $real_port,
      }
    } else {
      # This enabled this host to get WALs from the backup host
      # (We will want to have postgres::backup_server::register_backup_clienthost::allow_read_hosts set to something useful)
      include postgres::backup_source
    }
  }

  # hba entries and firewall rules
  Postgres::Cluster::Hba_entry <<| tag == "postgres::cluster::${real_version}::${real_cluster}::hba::${::fqdn}" |>>
  Postgres::Cluster::Hba_entry <<| tag == "postgres::cluster::${real_port}::hba::${::fqdn}" |>>

  if $manage_hba {
    concat { "postgres::cluster::${real_version}::${real_cluster}::hba":
      path           => "${confdir}/pg_hba.conf",
      mode           => '0440',
      group          => 'postgres',
      ensure_newline => true,
      notify         => Exec[$reload],
    }
    concat::fragment{ "postgres::cluster::pg_hba-head::${real_version}::${real_cluster}":
      target  => "postgres::cluster::${real_version}::${real_cluster}::hba",
      order   => '00',
      content => template('postgres/cluster/pg_hba.conf-head.erb'),
    }
    postgres::cluster::hba_entry { "postgres::cluster::${real_port}::local-connections":
      pg_port         => $real_port,
      connection_type => 'host',
      database        => 'all',
      user            => 'all',
      address         => ['127.0.0.1', '::1'],
      order           => '30',
      firewall        => false,
    }
    Concat::Fragment <| tag == "postgres::cluster::${real_version}::${real_cluster}::hba" |>
  }
}
