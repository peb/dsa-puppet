# our nagios server class
#
# it includes stored ferm configs for all the things it needs to access
# which are then collected by the monitored services
class nagios::server {
  include apache2
  include apache2::ssl
  include apache2::authn_anon

  ssl::service { 'nagios.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  apache2::site { '10-nagios.debian.org':
    site    => 'nagios.debian.org',
    content => template('nagios/nagios.debian.org.conf.erb'),
  }

  ensure_packages( [
    'icinga',
    'make',
    'monitoring-plugins',
    'nagios-nrpe-plugin',
    ], { ensure => 'installed' })

  service { 'icinga':
    ensure  => running,
    require => Package['icinga'],
  }

  file { '/etc/icinga/apache2.conf':
    content => template('nagios/icinga-apache2.conf.erb'),
    notify  => Exec['service apache2 reload'],
  }
  file { '/srv/nagios.debian.org/htpasswd':
    mode  => '0640',
    owner => 'root',
    group => 'www-data',
  }
  file { '/etc/icinga/cgi.cfg':
    ensure => symlink,
    target => 'config-pushed/static/cgi.cfg',
    notify => Exec['service apache2 reload'],
  }
  file { '/etc/icinga/icinga.cfg':
    ensure => symlink,
    target => 'config-pushed/static/icinga.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects':
    ensure  => directory,
    mode    => '0755',
    purge   => true,
    recurse => true,
    force   => true,
    source  => 'puppet:///files/empty/',
    notify  => Service['icinga'],
  }
  file { '/etc/icinga/objects/contacts.cfg':
    ensure => symlink,
    target => '../config-pushed/static/objects/contacts.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/generic-host.cfg':
    ensure => symlink,
    target => '../config-pushed/static/objects/generic-host.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/generic-service.cfg':
    ensure => symlink,
    target => '../config-pushed/static/objects/generic-service.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/timeperiods.cfg':
    ensure => symlink,
    target => '../config-pushed/static/objects/timeperiods.cfg',
    notify => Service['icinga'],
  }

  file { '/etc/icinga/objects/xauto-dependencies.cfg':
    ensure => symlink,
    target => '../config-pushed/generated/auto-dependencies.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/xauto-hostgroups.cfg':
    ensure => symlink,
    target => '../config-pushed/generated/auto-hostgroups.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/xauto-hosts.cfg':
    ensure => symlink,
    target => '../config-pushed/generated/auto-hosts.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/xauto-servicegroups.cfg':
    ensure => symlink,
    target => '../config-pushed/generated/auto-servicegroups.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/icinga/objects/xauto-services.cfg':
    ensure => symlink,
    target => '../config-pushed/generated/auto-services.cfg',
    notify => Service['icinga'],
  }

  file { '/etc/nagios-plugins/config/local-dsa-checkcommands.cfg':
    ensure => symlink,
    target => '../../icinga/config-pushed/static/checkcommands.cfg',
    notify => Service['icinga'],
  }
  file { '/etc/nagios-plugins/config/local-dsa-eventhandlers.cfg':
    ensure => symlink,
    target => '../../icinga/config-pushed/static/eventhandlers.cfg',
    notify => Service['icinga'],
  }

  file { '/etc/icinga/config-pushed':
    ensure => symlink,
    target => '/srv/nagios.debian.org/config-pushed'
  }

  file { '/srv/nagios.debian.org':
    ensure => directory,
    mode   => '0755',
  }
  file { '/srv/nagios.debian.org/config-pushed':
    ensure => directory,
    mode   => '0755',
    owner  => 'nagiosadm',
    group  => 'nagiosadm',
  }

  concat::fragment { 'puppet-crontab--nagios--restart-stale-icinga':
    target  => '/etc/cron.d/puppet-crontab',
    order   => '010',
    content => @(EOF)
      */15 * * * * root find /var/lib/icinga/status.dat -mmin +20 | grep -q . && service icinga restart
      | EOF
  }

  # The nagios server wants to do DNS queries on the primaries
  @@ferm::rule::simple { "dsa-bind-from-${::fqdn}":
    tag         => [
                    'named::primary::ferm',
                    'named::keyring::ferm',
    ],
    description => 'Allow nagios master access to the primary for checks',
    proto       => ['udp', 'tcp'],
    port        => 'domain',
    saddr       => $base::public_addresses,
  }

  # The nagios server wants to connect to the NRPE server on all the hosts
  @@ferm::rule::simple { "dsa-nrpe-from-${::fqdn}":
    tag         => 'nagios-nrpe::server',
    description => 'Allow nagios master access to the nrpe daemon',
    port        => '5666',
    saddr       => $base::public_addresses,
  }
  @@concat::fragment { "nrpe-debian-allow-${::fqdn}":
    tag     => 'nagios-nrpe::server::debianorg.cfg',
    target  => '/etc/nagios/nrpe.d/debianorg.cfg',
    content => "allowed_hosts=${ $base::public_addresses.join(', ') }",
  }
  # and we want to monitor smtp servers
  @@ferm::rule::simple { "dsa-smtp-from-nagios-${::fqdn}":
    tag         => 'smtp::server::to::mail-satellite',
    description => 'Allow smtp access from the nagios server',
    port        => '7', # will be overwritten on collection
    saddr       => $base::public_addresses,
  }
  # and we want to monitor ssh
  @@ferm::rule::simple { "dsa-ssh-from-nagios-${::fqdn}":
    tag         => 'ssh::server::from::nagios',
    description => 'Allow ssh access from the nagios server',
    chain       => 'ssh',
    saddr       => $base::public_addresses,
  }
}
