class grub(
  Boolean $grub_do_ifnames = ($::kernel == 'Linux' and $::is_virtual and $::virtual == 'kvm')
) {
  if ($::kernel == 'Linux' and $::is_virtual and $::virtual == 'kvm') {
    $grub_manage = true
    if !($::debarchitecture in [arm64, armhf, ppc64el]) {
      $grub_do_kernel_serial = true
      $grub_do_grub_serial = true
    }
  } elsif $::hostname in [ubc-enc2bl01,ubc-enc2bl02,ubc-enc2bl09,ubc-enc2bl10,casulana,mirror-anu,sallinen,storace,mirror-accumu,grnet-node03,grnet-node04,csail-node03,csail-node04,mirror-isc,mirror-umn,lobos,villa,clementi,czerny,lw01,lw02,lw03,lw04,lw07,lw08,lw09,lw10,wieck,schumann] {
    $grub_manage = true
    $grub_do_kernel_serial = true
    $grub_do_grub_serial = true
  } elsif $::hostname in [mirror-skroutz,conova-node01,conova-node02,conova-node03,conova-node04,arm-arm-01,fasolo,manda-node03,manda-node04,schmelzer,smit,klecker] {
    $grub_manage = true
    $grub_do_kernel_serial = true
    $grub_do_grub_serial = true
  } elsif $::hostname in [arm-arm-03] {
    $grub_manage = true
    $grub_do_kernel_serial = true
    $grub_do_grub_serial = false
  #} elsif $::hostname in [villa] {
  #  $grub_manage = true
  #  $grub_do_kernel_serial = false
  #  $grub_do_grub_serial = false
  } else {
    $grub_manage = false
  }

  if ($::update_grub and $grub_manage) {
    # hp-health requires nopat on linux 4.9
    $grub_do_nopat = ($::systemproductname and $::systemproductname =~ /^ProLiant/ and versioncmp($::kernelversion, '4.9') >= 0)

    $grub_do_pti_on = ($::debarchitecture == 'amd64' and versioncmp($::lsbmajdistrelease, '9') >= 0)

    $grub_do_extra = $::hostname in [fasolo]

    file { '/etc/default/grub':
      # restore to default
      source => 'puppet:///modules/grub/etc-default-grub',
      notify => Exec['update-grub']
    }

    file { '/etc/default/grub.d':
      ensure  => directory,
      mode    => '0555',
      purge   => true,
      force   => true,
      recurse => true,
      source  => 'puppet:///files/empty/',
    }

    file { '/etc/default/grub.d/puppet-grub-serial.cfg':
      ensure  => $grub_do_kernel_serial ? { true  => 'present', default => 'absent' },
      content => template('grub/puppet-grub-serial.cfg.erb'),
      notify  => Exec['update-grub']
    }

    file { '/etc/default/grub.d/puppet-kernel-serial.cfg':
      ensure  => $grub_do_grub_serial ? { true  => 'present', default => 'absent' },
      content => template('grub/puppet-kernel-serial.cfg.erb'),
      notify  => Exec['update-grub']
    }

    file { '/etc/default/grub.d/puppet-net-ifnames.cfg':
      ensure  => $grub_do_ifnames ? { true  => 'present', default => 'absent' },
      content => template('grub/puppet-net-ifnames.cfg.erb'),
      notify  => Exec['update-grub']
    }

    file { '/etc/default/grub.d/puppet-kernel-nopat.cfg':
      ensure  => $grub_do_nopat ? { true  => 'present', default => 'absent' },
      content => template('grub/puppet-kernel-nopat.cfg.erb'),
      notify  => Exec['update-grub']
    }

    file { '/etc/default/grub.d/puppet-kernel-extra.cfg':
      ensure  => $grub_do_extra ? { true  => 'present', default => 'absent' },
      content => template('grub/puppet-kernel-extra.cfg.erb'),
      notify  => Exec['update-grub']
    }

    file { '/etc/default/grub.d/puppet-kernel-pti-on.cfg':
      ensure  => $grub_do_pti_on ? { true  => 'present', default => 'absent' },
      content => template('grub/puppet-kernel-pti-on.cfg.erb'),
      notify  => Exec['update-grub']
    }
  }

  exec { 'update-grub':
    refreshonly => true,
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
  }
}
