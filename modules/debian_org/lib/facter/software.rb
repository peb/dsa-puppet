Facter.add("apache2") do
	setcode do
		if FileTest.exist?("/usr/sbin/apache2")
			true
		else
			false
		end
	end
end
Facter.add("clamd") do
	setcode do
		if FileTest.exist?("/usr/sbin/clamd")
			true
		else
			false
		end
	end
end
Facter.add("exim4") do
	setcode do
		if FileTest.exist?("/usr/sbin/exim4")
			true
		else
			false
		end
	end
end
Facter.add("exim4_ge_4_94") do
	setcode do
		system(%{test -n "$(dpkg-query -W -f='${Version}' exim4-base 2>/dev/null)" && dpkg --compare-versions "$(dpkg-query -W -f='${Version}' exim4-base)" ge 4.94})
	end
end
Facter.add("postfix") do
	setcode do
		if FileTest.exist?("/usr/sbin/postfix")
			true
		else
			false
		end
	end
end
Facter.add("postgres") do
	setcode do
		pg = (FileTest.exist?("/usr/lib/postgresql/9.1/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/9.4/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/9.6/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/11/bin/postgres") or
		FileTest.exist?("/usr/lib/postgresql/13/bin/postgres"))
		if pg
			true
		else
			false
		end
	end
end
Facter.add("postgrey") do
	setcode do
		if FileTest.exist?("/usr/sbin/postgrey")
			true
		else
			false
		end
	end
end
Facter.add("greylistd") do
	setcode do
		FileTest.exist?("/usr/sbin/greylistd")
	end
end
Facter.add("policydweight") do
	setcode do
		if FileTest.exist?("/usr/sbin/policyd-weight")
			true
		else
			false
		end
	end
end
Facter.add("spamd") do
	setcode do
		if FileTest.exist?("/usr/sbin/spamd")
			true
		else
			false
		end
	end
end
Facter.add("php5") do
	php =   (FileTest.exist?("/usr/lib/apache2/modules/libphp5.so") or
		FileTest.exist?("/usr/bin/php5") or
		FileTest.exist?("/usr/bin/php5-cgi") or
		FileTest.exist?("/usr/lib/cgi-bin/php5"))
	setcode do
		if php
			true
		else
			false
		end
	end
end
Facter.add("php5suhosin") do
	suhosin=(FileTest.exist?("/usr/lib/php5/20060613/suhosin.so") or
		FileTest.exist?("/usr/lib/php5/20060613+lfs/suhosin.so"))
	setcode do
		if suhosin
			true
		else
			false
		end
	end
end
Facter.add("syslogversion") do
	setcode do
		%x{dpkg-query -W -f='${Version}\n' syslog-ng | cut -d. -f1-2}.chomp
	end
end
Facter.add("unbound") do
	unbound=(FileTest.exist?("/usr/sbin/unbound") and
		FileTest.exist?("/var/lib/unbound/root.key"))
	setcode do
		if unbound
			true
		else
			false
		end
	end
end
Facter.add("samhain") do
	setcode do
		if FileTest.exist?("/usr/sbin/samhain")
			true
		else
			false
		end
	end
end
Facter.add("systemd") do
	setcode do
		init = '/sbin/init'
		if File.symlink?(init) and File.readlink(init) == "/lib/systemd/systemd"
			true
		else
			false
		end
	end
end
Facter.add("systemd_ge_245_4_2") do
	setcode do
		system(%{test -n "$(dpkg-query -W -f='${Version}' systemd 2>/dev/null)" && dpkg --compare-versions "$(dpkg-query -W -f='${Version}' systemd)" ge 245.4-2})
	end
end

Facter.add("tor_ge_0_2_9") do
	setcode do
		system(%{test -n "$(dpkg-query -W -f='${Version}' tor 2>/dev/null)" && dpkg --compare-versions "$(dpkg-query -W -f='${Version}' tor)" ge 0.2.9})
	end
end
Facter.add("haveged") do
	setcode do
		FileTest.exist?("/usr/sbin/haveged")
	end
end
Facter.add("bgpd") do
	setcode do
		FileTest.exist?("/usr/sbin/bgpd")
	end
end
Facter.add("zebra") do
	setcode do
		FileTest.exist?("/usr/sbin/zebra")
	end
end
Facter.add("update_grub") do
	setcode do
		FileTest.exist?("/usr/sbin/update-grub")
	end
end
Facter.add("haproxy") do
	setcode do
		FileTest.exist?("/usr/sbin/haproxy")
	end
end
Facter.add("smartd") do
	setcode do
		FileTest.exist?("/usr/sbin/smartd")
	end
end
Facter.add("etckeeper") do
	setcode do
		FileTest.exist?("/usr/bin/etckeeper")
	end
end
