# == Class: debian_org
#
# Stuff common to all debian.org servers
#
class debian_org {
  include debian_org::apt

  if $::systemd {
    include dsa_systemd
    $servicefiles = 'present'
  } else {
    $servicefiles = 'absent'
  }

  # the virtual facter needs virt-what on jessie to work; clean up.
  package { 'virt-what': ensure => purged }

  $samhain_recipients = hiera('samhain_recipients')
  $root_mail_alias = hiera('root_mail_alias')

  package { [
      'klogd',
      'sysklogd',
      'rsyslog',
      'os-prober',
      'apt-listchanges',
      'mlocate',
    ]:
    ensure => purged,
  }
  package { [
      'debian.org',
      'debian.org-recommended',
      'userdir-ldap',
    ]:
    ensure => installed,
    tag    => extra_repo,
  }
  ensure_packages ( [
      'dsa-munin-plugins',
    ], { ensure => purged }
  )

  package { [
      'apt-utils',
      'bash-completion',
      'dnsutils',
      'less',
      'lsb-release',
      'ruby-filesystem',
      'mtr-tiny',
      'nload',
      'pciutils',
      'lldpd',
      'ncurses-term',
    ]:
    ensure => installed,
  }

  munin::check { [
      'cpu',
      'entropy',
      'forks',
      'interrupts',
      'iostat',
      'irqstats',
      'load',
      'memory',
      'open_files',
      'open_inodes',
      'processes',
      'swap',
      'uptime',
      'vmstat',
    ]:
  }

  package { 'molly-guard':
    ensure => installed,
  }
  file { '/etc/login.defs':
    source  => 'puppet:///modules/debian_org/login.defs',
  }
  file { '/etc/molly-guard/run.d/10-check-kvm':
    mode    => '0755',
    source  => 'puppet:///modules/debian_org/molly-guard/10-check-kvm',
    require => Package['molly-guard'],
  }
  file { '/etc/molly-guard/run.d/15-acquire-reboot-lock':
    mode    => '0755',
    source  => 'puppet:///modules/debian_org/molly-guard/15-acquire-reboot-lock',
    require => Package['molly-guard'],
  }

  augeas { 'inittab_replicate':
    context => '/files/etc/inittab',
    changes => [
      'set ud/runlevels 2345',
      'set ud/action respawn',
      'set ud/process "/usr/bin/ud-replicated -d"',
    ],
    notify  => Exec['init q'],
  }


  file { '/etc/facter':
    ensure  => directory,
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///files/empty/',
  }
  file { '/etc/facter/facts.d':
    ensure => directory,
  }
  file { '/etc/facter/facts.d/debian_facts.yaml':
    content => template('debian_org/debian_facts.yaml.erb')
  }
  file { '/etc/timezone':
    content => "Etc/UTC\n",
    notify  => Exec['dpkg-reconfigure tzdata -pcritical -fnoninteractive'],
  }
  file { '/etc/localtime':
    ensure => 'link',
    target => '/usr/share/zoneinfo/Etc/UTC',
    notify => Exec['dpkg-reconfigure tzdata -pcritical -fnoninteractive'],
  }
  file { '/etc/puppet/puppet.conf':
    content => template('debian_org/puppet.conf.erb'),
    mode    => '0440',
    group   => 'puppet',
  }
  file { '/etc/default/puppet':
    source => 'puppet:///modules/debian_org/puppet.default',
  }
  file { '/etc/systemd':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/systemd/system':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/systemd/system/ud-replicated.service':
    ensure => $servicefiles,
    source => 'puppet:///modules/debian_org/ud-replicated.service',
    notify => Exec['systemctl daemon-reload'],
  }
  if $::systemd {
    file { '/etc/systemd/system/multi-user.target.wants/ud-replicated.service':
      ensure => 'link',
      target => '../ud-replicated.service',
      notify => Exec['systemctl daemon-reload'],
    }
  }
  file { '/etc/systemd/system/puppet.service':
    ensure => 'link',
    target => '/dev/null',
    notify => Exec['systemctl daemon-reload'],
  }
  file { '/etc/systemd/system/proc-sys-fs-binfmt_misc.automount':
    ensure => 'link',
    target => '/dev/null',
    notify => Exec['systemctl daemon-reload'],
  }

  file { '/etc/cron.d/dsa-puppet-stuff':
    ensure => 'absent',
  }
  file { '/etc/dsa/cron.ignore.dsa-puppet-stuff':
    ensure => 'absent',
  }

  file { '/usr/local':
    ensure => directory,
    mode   => '0755',
  }
  file { '/usr/local/bin':
    ensure => directory,
    mode   => '0755',
  }
  file { '/usr/local/sbin':
    ensure => directory,
    mode   => '0755',
  }

  concat { '/etc/cron.d/puppet-crontab': }
  concat::fragment { 'puppet-crontab---header':
    target  => '/etc/cron.d/puppet-crontab',
    order   => '000',
    content => @(EOF)
      ## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
      SHELL=/bin/bash
      MAILTO=root
      PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/nagios/plugins
      | EOF
  }
  concat::fragment { 'puppet-crontab---all':
    target  => '/etc/cron.d/puppet-crontab',
    order   => '010',
    content => template('debian_org/puppet-crontab.cron.erb'),
    require => Package['debian.org'],
  }
  file { '/etc/ldap/ldap.conf':
    require => Package['debian.org'],
    content => template('debian_org/ldap.conf.erb'),
  }
  file { '/etc/pam.d/common-session':
    require => Package['debian.org'],
    content => template('debian_org/pam.common-session.erb'),
  }
  file { '/etc/pam.d/common-session-noninteractive':
    require => Package['debian.org'],
    content => template('debian_org/pam.common-session-noninteractive.erb'),
  }
  file { '/etc/rc.local':
    mode    => '0755',
    content => template('debian_org/rc.local.erb'),
    notify  => Exec['service rc.local restart'],
  }
  file { '/etc/dsa':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/dsa/cron.ignore.puppet-crontab':
    source  => 'puppet:///modules/debian_org/puppet-crontab.cron.ignore',
    require => Package['debian.org']
  }
  file { '/etc/nsswitch.conf':
    mode   => '0444',
    source => 'puppet:///modules/debian_org/nsswitch.conf',
  }

  file { '/etc/profile.d/timeout.sh':
    mode   => '0555',
    source => 'puppet:///modules/debian_org/etc.profile.d/timeout.sh',
  }
  file { '/etc/zsh':
    ensure => directory,
  }
  file { '/etc/zsh/zprofile':
    mode   => '0444',
    source => 'puppet:///modules/debian_org/etc.zsh/zprofile',
  }
  file { '/etc/environment':
    content => '',
    mode    => '0440',
  }
  file { '/etc/default/locale':
    content => '',
    mode    => '0444',
  }

  # set mmap_min_addr to 4096 to mitigate
  # Linux NULL-pointer dereference exploits
  base::sysctl { 'mmap_min_addr':
    ensure => absent
  }
  base::sysctl { 'perf_event_paranoid':
    key   => 'kernel.perf_event_paranoid',
    value => '2',
  }
  base::sysctl { 'puppet-vfs_cache_pressure':
    key   => 'vm.vfs_cache_pressure',
    value => '10',
  }
  base::alternative { 'editor':
    linkto => '/usr/bin/vim.basic',
  }
  base::alternative { 'view':
    linkto => '/usr/bin/vim.basic',
  }

  if (versioncmp($::lsbmajdistrelease, '11') >= 0) {
    base::alternative { 'iptables':
      linkto => '/usr/sbin/iptables-legacy',
    }
    base::alternative { 'ip6tables':
      linkto => '/usr/sbin/ip6tables-legacy',
    }
  }

  mailalias { 'samhain-reports':
    ensure    => present,
    recipient => $samhain_recipients,
    require   => Package['debian.org']
  }
  mailalias { 'root':
    ensure    => present,
    recipient => $root_mail_alias,
    require   => Package['debian.org']
  }

  file { '/usr/local/bin/check_for_updates':
    source => 'puppet:///modules/debian_org/check_for_updates',
    mode   => '0755',
    owner  => root,
    group  => root,
  }
  file { '/usr/local/bin/dsa-is-shutdown-scheduled':
    ensure => 'absent',
  }

  exec { 'dpkg-reconfigure tzdata -pcritical -fnoninteractive':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    refreshonly => true
  }
  exec { 'service puppetmaster restart':
    refreshonly => true
  }
  exec { 'service rc.local restart':
    refreshonly => true
  }
  exec { 'init q':
    refreshonly => true
  }

  exec { 'systemctl daemon-reload':
    refreshonly => true,
    onlyif      => 'test -x /bin/systemctl'
  }

  exec { 'systemd-tmpfiles --create --exclude-prefix=/dev':
    refreshonly => true,
    onlyif      => 'test -x /bin/systemd-tmpfiles'
  }

  tidy { '/var/lib/puppet/clientbucket/':
    age      => '2w',
    recurse  => 9,
    type     => ctime,
    matches  => [ 'paths', 'contents' ],
    schedule => weekly
  }

  file { '/root/.bashrc':
    source => 'puppet:///modules/debian_org/root-dotfiles/bashrc',
  }
  file { '/root/.profile':
    source => 'puppet:///modules/debian_org/root-dotfiles/profile',
  }
  file { '/root/.selected_editor':
    source => 'puppet:///modules/debian_org/root-dotfiles/selected_editor',
  }
  file { '/root/.screenrc':
    source => 'puppet:///modules/debian_org/root-dotfiles/screenrc',
  }
  file { '/root/.tmux.conf':
    source => 'puppet:///modules/debian_org/root-dotfiles/tmux.conf',
  }
  file { '/root/.vimrc':
    source => 'puppet:///modules/debian_org/root-dotfiles/vimrc',
  }

  if versioncmp($::lsbmajdistrelease, '9') == 0 { # older puppets do facts as strings.
    if $::processorcount > 1 {
      package { 'irqbalance': ensure => installed }
    }
  } else {
    # 926967 drops the recommendation on irqbalance in Buster
    package { 'irqbalance': ensure => purged }
  }


  # https://www.decadent.org.uk/ben/blog/bpf-security-issues-in-debian.html
  base::sysctl { 'unprivileged_bpf_disabled':
    key   => 'kernel.unprivileged_bpf_disabled',
    value => '1',
  }

  # our ipv6 addresses and routes are statically configured.
  # needs to be late so it runs after other scripts like vlan which actually
  # create the interface
  file { '/etc/network/if-pre-up.d/zz_no_accept_ra':
    source => 'puppet:///modules/debian_org/ifupdown-pre-up-accept-ra',
    mode   => '0555',
  }
  file { '/etc/network/if-pre-up.d/no_accept_ra':
    ensure => absent,
  }
  base::sysctl { 'dsa-accept-ra-default':
    ensure => absent,
  }
  base::sysctl { 'dsa-accept-ra-all':
    ensure => absent,
  }

  # Disable kpartx udev rules
  $want_kpartx_rules = $::has_lib_udev_rules_d_60_kpartx_rules ? { true  => 'present', default => 'absent' }
  file { '/etc/udev/rules.d/60-kpartx.rules':
    ensure  => $want_kpartx_rules,
    content => '',
    mode    => '0444',
  }

  # this is only to avoid warnings, else puppet will complain that we
  # have a symlink there, even if we're not replacing it anyhow.
  if ! $::has_etc_ssh_ssh_known_hosts {
    file { '/etc/ssh/ssh_known_hosts':
      ensure  => 'present',
      replace => 'no',
      content => inline_template('<%= open("/etc/ssh/ssh_known_hosts").read() %>'),
      notify  => Exec['ud-replicate'],
    }
  }

  exec { 'ud-replicate':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => '/usr/bin/ud-replicate',
    refreshonly => true,
    require     => Package['userdir-ldap']
  }

  # some changes require rebuilding the initramfs.  Have the common exec here.
  exec { 'update-initramfs -u':
    path        => '/etc/init.d:/usr/bin:/usr/sbin:/bin:/sbin',
    refreshonly => true;
  }
}
