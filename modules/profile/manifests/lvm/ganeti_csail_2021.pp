# LVM config for the lenovo x86 servers that make up ganeti.csail.debian.org
class profile::lvm::ganeti_csail_2021 {
  class { 'dsalvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
