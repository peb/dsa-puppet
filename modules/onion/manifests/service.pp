# Provide an onion service
#
# @param port           The port to "listen" on, i.e. the port of the onion service
# @param target_address IP Address or hostname of the backing service
# @param target_port    TCP Port of the backing service
# @param direct         Will clients access this directly (true) or via onionbalance (false)
# @param ensure         present, absent, or ifstatic
define onion::service (
  Stdlib::Port $port,
  Stdlib::Host $target_address,
  Stdlib::Port $target_port,
  Enum['present', 'absent', 'ifstatic'] $ensure = present,
  Boolean $direct = false
) {
  if ($ensure == 'ifstatic') {
    $my_ensure = has_static_component($name) ? {
      true  => 'present',
      false => 'absent'
    }
  } else {
    $my_ensure = $ensure
  }

  if ($my_ensure == 'present') {
    include onion

    # hostnames for this onion service at the local tor instance
    $onion_hn = onion_tor_service_hostname($name)
    $onionv3_hn = onionv3_tor_service_hostname($name)
    # end-user visible hostname.  Which might be different if this is done via onionbalance.
    $onionv3_global_hn = onionv3_global_service_hostname($name)

    $am_v3_onionbalance_backend = (! $direct and $onionv3_hn and $onionv3_global_hn and $onionv3_hn != $onionv3_global_hn);
    concat::fragment { "onion::torrc_onionservice::${name}":
      target  => '/etc/tor/torrc',
      order   => '50',
      content => inline_template( @(EOF) ),
        HiddenServiceDir /var/lib/tor/onion/<%= @name %>
        HiddenServiceVersion 2
        HiddenServicePort <%= @port %> <%= @target_address %>:<%= @target_port %>

        HiddenServiceDir /var/lib/tor/onionv3/<%= @name %>
        HiddenServiceVersion 3
        HiddenServicePort <%= @port %> <%= @target_address %>:<%= @target_port %>
        <% if @am_v3_onionbalance_backend %>
        HiddenServiceOnionBalanceInstance 1
        <% end %>

        | EOF
    }
    if ($am_v3_onionbalance_backend) {
      file { "/var/lib/tor/onionv3/${name}/ob_config":
        notify  => Exec['service tor reload'],
        content => @("EOF"),
          MasterOnionAddress ${onionv3_global_hn}
          | EOF
      }
    } else {
      file { "/var/lib/tor/onionv3/${name}/ob_config":
        ensure => absent,
        notify => Exec['service tor reload'],
      }
    }

    # v2 onions
    if $onion_hn {
      $hostname_without_onion = regsubst($onion_hn, '\.onion$', '')

      if ($direct) {
        @@concat::fragment { "onion::balance::${::hostname}::onionbalance-services.yaml::${name}":
          target  => '/srv/puppet.debian.org/puppet-facts/onionbalance-services.yaml',
          content => "{\"${name}\": \"${onion_hn}\"}\n",
          tag     => 'onionbalance-services.yaml',
        }
      } else {
        @@concat::fragment { "onion::balance::instance::dsa-snippet::${name}::${::fqdn}":
          target  => '/etc/onionbalance/config-dsa-snippet.yaml',
          content => "- service: ${name}\n  address: ${hostname_without_onion}\n  name: ${::hostname}-${name}\n",
          tag     => 'onion::balance::dsa-snippet',
        }
      }
    }
    # v3 onions
    if $onionv3_hn {
      $hostname_without_onionv3 = regsubst($onionv3_hn, '\.onion$', '')

      if ($direct) {
        @@concat::fragment { "onion::balance::${::hostname}::onionbalancev3-services.yaml::${name}":
          target  => '/srv/puppet.debian.org/puppet-facts/onionbalancev3-services.yaml',
          content => "{\"${name}\": \"${onionv3_hn}\"}\n",
          tag     => 'onionbalancev3-services.yaml',
        }
      } else {
        @@concat::fragment { "onion::balance::instance::dsa-snippetv3::${name}::${::fqdn}":
          target  => '/etc/onionbalance/config-dsa-snippetv3.yaml',
          content => "- service: ${name}\n  address: ${hostname_without_onionv3}\n  name: ${::hostname}-${name}\n",
          tag     => 'onion::balance::dsa-snippetv3',
        }
      }
    }
  } elsif ($my_ensure == 'absent') {
    file { "/var/lib/tor/onion/${name}":
      ensure => absent,
      force  => true,
    }
    file { "/var/lib/tor/onionv3/${name}":
      ensure => absent,
      force  => true,
    }
  } else {
    fail("Invalid ensure value ${my_ensure}")
  }
}
