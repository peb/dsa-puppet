# = Class: ganeti2::params
#
# Parameters for ganeti clusters
#
# == Sample Usage:
#
#   include ganeti2::params
#
class ganeti2::params {
  include stdlib

  case $::cluster {
    'ganeti-osuosl.debian.org': {
      $ganeti_hosts = ['140.211.166.20/32']
      $ganeti_priv  = ['140.211.166.20/32']
      $drbd         = false
    }
    'ganeti2-osuosl.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'pijper.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'pieta.debian.org', 'ipHostNumber')
      $ganeti_priv  = getfromhash($deprecated::allnodeinfo, 'pijper.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'pieta.debian.org', 'ipHostNumber')
      $drbd         = true
    }
    'ganeti.manda.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'manda-node03.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'manda-node04.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.182.13', '172.29.182.14']
      $drbd         = true
    }
    'ganeti2.ubc.debian.org': {
      $ganeti_hosts = ['209.87.16.1/32', '209.87.16.2/32', '209.87.16.3/32', '209.87.16.4/32', '209.87.16.5/32', '209.87.16.6/32', '209.87.16.7/32', '209.87.16.8/32', '209.87.16.9/32', '209.87.16.10/32', '209.87.16.11/32', '209.87.16.12/32', '209.87.16.13/32', '209.87.16.14/32', '209.87.16.15/32', '209.87.16.16/32']
      $ganeti_priv  = ['172.29.42.1/32', '172.29.42.2/32', '172.29.42.3/32', '172.29.42.4/32', '172.29.42.5/32', '172.29.42.6/32', '172.29.42.7/32', '172.29.42.8/32', '172.29.42.9/32', '172.29.42.10/32', '172.29.42.11/32', '172.29.42.12/32', '172.29.42.13/32', '172.29.42.14/32', '172.29.42.15/32', '172.29.42.16/32']
      $drbd         = false
    }
    'ganeti.csail.debian.org': {
      $ganeti_hosts = ['128.31.0.19/32', '128.31.0.20/32']
      $ganeti_priv  = ['172.29.178.0/24']
      $drbd         = true
    }
    'ganeti.grnet.debian.org': {
      $ganeti_hosts = ['194.177.211.197/32', '194.177.211.198/32']
      $ganeti_priv  = ['172.29.175.0/24']
      $drbd         = true
    }
    'ganeti-conova.debian.org': {
      $ganeti_hosts = ['217.196.149.227/32', '217.196.149.228/32']
      $ganeti_priv  = ['172.29.184.11/32', '172.29.184.12/32']
      $drbd         = true
    }
    'ganeti3.ubc.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'ubc-node-arm04.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'ubc-node-arm05.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'ubc-node-arm06.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.42.102', '172.29.42.112', '172.29.42.122']
      $drbd         = true
    }
    'ganeti2.conova.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'conova-node03.debian.org', 'ipHostNumber') +
                                        getfromhash($deprecated::allnodeinfo, 'conova-node04.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.185.3/32', '172.29.185.4/32']
      $drbd         = true
    }
    default: {
      $ganeti_hosts = []
      $ganeti_priv  = []
      $drbd         = false
    }
  }
}
