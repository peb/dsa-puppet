# = Class: ganeti2
#
# Standard ganeti2 config debian.org hosts
#
# == Sample Usage:
#
#   include ganeti2
#
class ganeti2 {

  include ganeti2::params
  include ganeti2::firewall

  $drbd = $ganeti2::params::drbd

  package { 'ganeti':
    ensure => installed
  }

  base::linux_module { 'tun': }

  file { '/etc/cron.hourly/puppet-cleanup-watcher-pause-file':
    source => 'puppet:///modules/ganeti2/cleanup-watcher-pause-file',
    mode   => '0555',
  }

  if $::debarchitecture == 'arm64' {
    file { '/usr/local/bin/qemu-system-aarch64-wrapper':
      source => 'puppet:///modules/ganeti2/qemu-system-aarch64-wrapper',
      mode   => '0555',
    }
  }
  file { '/usr/local/sbin/ganeti-reboot-cluster':
    source => 'puppet:///modules/ganeti2/ganeti-reboot-cluster',
    mode   => '0555',
  }

  if (versioncmp($::lsbmajdistrelease, '11') >= 0) {
    package { ['python3-dbus', 'systemd-container']: ensure => installed }
    file { '/usr/local/sbin/ganeti-machined-register-instances':
      source => 'puppet:///modules/ganeti2/ganeti-machined-register-instances.py3',
      mode   => '0555',
    }
  } else {
    package { ['python-dbus', 'systemd-container']: ensure => installed }
    file { '/usr/local/sbin/ganeti-machined-register-instances':
      source => 'puppet:///modules/ganeti2/ganeti-machined-register-instances.py2',
      mode   => '0555',
    }
  }

  file { [
    '/etc/ganeti/hooks',
    '/etc/ganeti/hooks/instance-reboot-post.d',
    '/etc/ganeti/hooks/instance-migrate-post.d',
    '/etc/ganeti/hooks/instance-start-post.d',
    '/etc/ganeti/hooks/instance-failover-post.d',
    '/etc/ganeti/hooks/instance-add-post.d',
    ]:
    ensure => directory,
  }
  file { [
    '/etc/ganeti/hooks/instance-reboot-post.d/00-ganeti-machined-register-instances',
    '/etc/ganeti/hooks/instance-migrate-post.d/00-ganeti-machined-register-instances',
    '/etc/ganeti/hooks/instance-start-post.d/00-ganeti-machined-register-instances',
    '/etc/ganeti/hooks/instance-failover-post.d/00-ganeti-machined-register-instances',
    '/etc/ganeti/hooks/instance-add-post.d/00-ganeti-machined-register-instances',
    ]:
    ensure => link,
    target => '/usr/local/sbin/ganeti-machined-register-instances',
  }

  file { '/etc/sudoers.d/ganeti':
    mode    => '0440',
    content => template('ganeti2/sudoers.erb'),
  }

  # while we test this, limit to manda
  range(0, 39).each |$i| {
    $num = sprintf('%02d', $i)
    $uidname = "ganeti${num}"
    $uidnum = 29400 + $i

    group { $uidname:
      gid => $uidnum,
    }
    user { $uidname:
      uid     => $uidnum,
      gid     => $uidnum,
      home    => '/nonexistent',
      shell   => '/usr/sbin/nologin',
      comment => 'ganeti pool user',
    }
  }
}
